#include "Triangle.hpp"
#include "Plan.hpp"

// choix de l'algorithme de calcul d'intersection entre
// un rayon et un triangle. Il faut décommenter la constante
// correspondant à l'algorithme choisi, puis recompiler l'application.

#define MOLLER
//#define BASIC

// Activation du test de "backface culling"
// Si la constante est commentée, le test n'a pas lieu
// Recompiler l'application à chaque changement d'état de la constante
//#define TEST_CULL

Triangle::Triangle() : Objet(){
  s[0].set(-1, 0, -1);
  s[1].set(1, 0, -1);
  s[2].set(0, 0, 1);
  n.set(0, 0, 1);
}

Triangle::Triangle(const Point p[3], Materiau m) : Objet(m) {
  for(int i=0; i<3; i++)
    s[i] = p[i];
  // calcul de la normale à partir du produit vectoriel AB^AC
  // cette normale doit ensuite être normalisée...
  n = Vecteur::cross(Vecteur(s[0], s[1]), Vecteur(s[0], s[2]));
  n.normaliser();
}

Triangle::~Triangle(){}

// Implémentation de l'algorithme de Moller et Tumbore pour le calcul
// d'intersection entre un rayon et un triangle.
// L'agorithme est implémenté dans la méthode d'intersection et
// dans celle de coupe pour les rayons d'ombrage
#ifdef MOLLER
bool Triangle::intersecte(const Rayon& r, Intersection& inter){

  const float TRI_EPSILON = 0.0001;

  // find vectors for tvo edges sharing vertO (s[0])
  Vecteur edge1(s[0], s[1]);
  Vecteur edge2(s[0], s[2]);

  // begin calculating determinant - also used to calculate U (u) parameter
  Vecteur pvec = Vecteur::cross(r.direction, edge2);

  // if determinant is near zero, ray lies in plane of triangle
  float det = edge1*pvec;

#ifdef TEST_CULL // define TEST_CULL if culling is desired
  if(det < TRI_EPSILON) return false;

    // calculate distance from vertO (s[0]) to ray origin
  Vecteur tvec(s[0], r.origine);

  // calculate U parameter and test bounds
  float u = (tvec*pvec);
  if(u < 0.0 || u > det) return false;

  // prepare to test V parameter
  Vecteur qvec = Vecteur::cross(tvec,edge1);

  // calculate V parameter and test bounds
  float v = (qvec * r.direction);
  if(v < 0.0 || u + v > det) return false;

  // calculate t, ray intersects triangle
  float t = (qvec*edge2);
  float inv_det = 1.0 / det;
  t *= inv_det;
  u *= inv_det;
  v *= inv_det;

#else // the non-culling branch
  if (det > -TRI_EPSILON && det < TRI_EPSILON)
    return false;    // Le rayon est parallèle au triangle.

  float inv_det = 1.0/det;

  // calculate distance from vertO (s[0]) to ray origin
  Vecteur tvec(s[0], r.origine);

  // calculate U parameter and test bounds
  float u = inv_det * (tvec*pvec);
  if (u < 0.0 || u > 1.0)
    return false;

  // prepare to test V (v) parameter
  Vecteur qvec = Vecteur::cross(tvec,edge1);

  // calculate V (v) parameter and test bounds
  float v = inv_det * (qvec * r.direction);
  if (v < 0.0 || u + v > 1.0)
    return false;

  // calculate t, ray intersects triangle
  float t = inv_det * (edge2 * qvec);
#endif

  // Calculer le point d'intersection si celle-ci est vers "l'avant"
  // du rayon. Sinon, l'intersection est derrière son l'origine de celui-ci
  if (t > TRI_EPSILON){ // Intersection avec le rayon
    inter = Intersection(Point (r.origine.X + t*r.direction.dx,
				r.origine.Y + t*r.direction.dy,
				r.origine.Z + t*r.direction.dz),
			 this, t);;
    return true;
  } else // L'intersection est derrière l'origine du rayon
    return false;

}


bool Triangle::coupe(const Rayon& r){

  const float TRI_EPSILON = 0.0001;

  // find vectors for tvo edges sharing vertO (s[0])
  Vecteur edge1(s[0], s[1]);
  Vecteur edge2(s[0], s[2]);

  // begin calculating determinant - also used to calculate U (u) parameter
  Vecteur pvec = Vecteur::cross(r.direction, edge2);

  // if determinant is near zero, ray lies in plane of triangle
  float det = edge1*pvec;

#ifdef TEST_CULL // define TEST_CULL if culling is desired
  if(det < TRI_EPSILON) return false;

  // calculate distance from vertO (s[0]) to ray origin
  Vecteur tvec(s[0], r.origine);

  // calculate U parameter and test bounds
  float u = (tvec*pvec);
  if(u < 0.0 || u > det) return false;

  // prepare to test V parameter
  Vecteur qvec = Vecteur::cross(tvec,edge1);

  // calculate V parameter and test bounds
  float v = (qvec * r.direction);
  if(v < 0.0 || u + v > det) return false;

  // calculate t, ray intersects triangle
  float t = (qvec*edge2);
  t /= det;

#else  // the non-culling branch
  if (det > -TRI_EPSILON && det < TRI_EPSILON)
    return false;    // Le rayon est parallèle au triangle.

  float inv_det = 1.0/det;

  // calculate distance from vertO (s[0]) to ray origin
  Vecteur tvec(s[0], r.origine);

  // calculate U parameter and test bounds
  float u = inv_det * (tvec*pvec);
  if (u < 0.0 || u > 1.0)
    return false;

  // prepare to test V (v) parameter
  Vecteur qvec = Vecteur::cross(tvec,edge1);

  // calculate V (v) parameter and test bounds
  float v = inv_det * (qvec * r.direction);
  if (v < 0.0 || u + v > 1.0)
    return false;
    // calculate t, ray intersects triangle
  float t = inv_det * (edge2 * qvec);
#endif


  // Calculer le point d'intersection si celle-ci est vers "l'avant"
  // du rayon. Sinon, l'intersection est derrière son l'origine de celui-ci
  if (t > TRI_EPSILON && t<1.0) return true;
  return false;
}

#endif

// Implémentation de l'algorithme basique de calcul
// d'intersection entre un rayon et un triangle.
// L'agorithme est implémenté dans la méthode d'intersection et
// dans celle de coupe pour les rayons d'ombrage
#ifdef BASIC
bool Triangle::intersecte(const Rayon& r, Intersection& inter){
   const float TRI_EPSILON = 0.000001;
 
  // calcul du dernier coefficient du plan ax+by+cz=-d
  float d = -(n.dx*s[0].X+n.dy*s[0].Y+n.dz*s[0].Z);

  // calcul de l'intersection avec le plan support
  // Attention : ne pas passer par la classe Plan
  // car l'intersection appartiendrait au plan, pas au triangle ...
  
  Point o = r.origine;
  Vecteur dir = r.direction;
  
  // calcul du dénominateur

  float denominateur = n.dx*dir.dx + n.dy*dir.dy + n.dz*dir.dz;

#ifdef TEST_CULL
  if(denominateur >= 0) return false;
#else
  if(denominateur == 0) return false;
#endif

  // calcul du numerateur
  float numerateur = n.dx*o.X + n.dy*o.Y + n.dz*o.Z + d;

  float t= -numerateur/denominateur;
  if(t<TRI_EPSILON) return false;

  // calcul du point d'intersection
  inter = Intersection(Point(o.X+t*dir.dx,
			     o.Y+t*dir.dy,
			     o.Z+t*dir.dz),
		       this, t);

  // le point d'intersection est-il à l'intérieur du triangle ?
  
  if(Vecteur::cross(Vecteur(s[0],s[1]),Vecteur(s[0], inter))*n<TRI_EPSILON) return false;
  if(Vecteur::cross(Vecteur(s[1],s[2]),Vecteur(s[1], inter))*n<TRI_EPSILON) return false;
  if(Vecteur::cross(Vecteur(s[2],s[0]),Vecteur(s[2], inter))*n<TRI_EPSILON) return false;

  return true;
  
}

bool Triangle::coupe(const Rayon& r){
  Intersection tmp;
  if(intersecte(r, tmp) && tmp.getDistance()<=1.0) return true;
  return false;
}
#endif





Vecteur Triangle::getNormale(const Point &p){
  return n;
}

ostream& operator<<(ostream & sortie, Triangle & t){
  sortie << "triangle : ";
  for(int i=0; i<3; i++)
    sortie << t.s[i] << " - ";
  sortie << endl;

  return sortie;
    
}

void Triangle::affiche(ostream& out){
  out << "triangle : ";
  for(int i=0; i<3; i++)
    out << s[i] << " - ";
  out << endl;
}

